#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/8/23 14:07
# @Author  : 一叶知秋
# @File    : simple2.py
# @Software: PyCharm
import tesserocr
from PIL import Image

image = Image.open('image.png')
print(tesserocr.image_to_text(image))

print("-" * 50)
print(tesserocr.file_to_text('image.png'))
